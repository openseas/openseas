using UnityEngine;
using System.Collections;

public class Buoyancy : MonoBehaviour {
    public float UpwardForce = 11f; // 9.81 is the opposite of the default gravity, which is 9.81. If we want the boat not to behave like a submarine the upward force has to be higher than the gravity in order to push the boat to the surface
	public float waterLevel = 0f; 

   

    void FixedUpdate() 
	{
		if(transform.position.y < waterLevel)
		{
			Vector3 force = transform.up * UpwardForce;
            this.rigidbody.AddRelativeForce(force, ForceMode.Acceleration);
            //Debug.Log("Upward force: " + force+" @"+Time.time);

        }

    }
}