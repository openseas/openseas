﻿using UnityEngine;
using System.Collections;

public class CannonBall : MonoBehaviour {

	private float born;
	public float timeToLive = 8.0f;
	public GameObject explosion;
	public Component script;

	// Use this for initializationd
	void Start () {
		born = Time.time;
	}
	
	// Update is called once per frame
	void Update () 
	{
		if(Time.time - born > timeToLive)
		{
			Destroy(this.gameObject);
		}

		if (transform.position.y < 0)
			Destroy (this.gameObject,.1f);

	}

	void OnCollisionEnter(Collision other)
	{
		Instantiate(explosion, transform.position, transform.rotation);
		if(other.gameObject.tag == "Destroyable")
		{

			if(other.gameObject.name == "EnemyShip")
			{
				other.gameObject.GetComponent<Buoyancy>().UpwardForce = 7.0f;
			}
			Destroy(other.gameObject,timeToLive);

		}
			
		Destroy(this.gameObject);

	}
}
