﻿using UnityEngine;
using System.Collections;

public class BoatController : MonoBehaviour {
	public float speed = 1f;
	public float rotateSpeed = 1f;
	public float maxSpeed = 100f;
	public float currentSpeed;
	public Vector3 force;
	public Vector3 cameraStartPos;
	public Vector3 cameraStartRot;
	public GameObject rightCannon;
	public GameObject leftCannon;
	public bool hasCannons;
	// Use this for initialization
	void Start () 
	{
		hasCannons = false;
		cameraStartPos = Camera.main.transform.localPosition;
		cameraStartRot = Camera.main.transform.localEulerAngles;
	}
	
	// Update is called once per frame
	void Update () 
	{
		 force = new Vector3(0,0,Input.GetAxis("Vertical"));
		currentSpeed = rigidbody.velocity.magnitude;
		if(currentSpeed < maxSpeed )
			rigidbody.AddRelativeForce(force * Time.deltaTime * speed * rigidbody.mass);

		//rigidbody.AddForce(force * Time.deltaTime * speed);

		Vector3 rotate = new Vector3(0,Input.GetAxis("Horizontal"),0);

		transform.Rotate(rotate * Time.deltaTime * rotateSpeed);

		if(Input.GetMouseButton(1))
		{

			Camera.main.transform.localPosition = rightCannon.transform.localPosition;
			Camera.main.transform.localEulerAngles = rightCannon.transform.localEulerAngles;
		}

		else if(Input.GetMouseButton(0))
		{
			Camera.main.transform.localPosition = leftCannon.transform.localPosition;
			Camera.main.transform.localEulerAngles = leftCannon.transform.localEulerAngles;
		}

		else
		{
			Camera.main.transform.localPosition = cameraStartPos;
			Camera.main.transform.localEulerAngles = cameraStartRot;
		}
		
	}


}
