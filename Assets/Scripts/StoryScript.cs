﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class StoryScript : MonoBehaviour {

	public int missionNumber;
	public Text mission;
	public Text location;
	public Text money ;
	public int moneyCount;
	public NeedleControler destArrow;
	public GameObject england;
	public GameObject france;
	public GameObject spain;
	public GameObject islandCove;


	// Use this for initialization
	void Start () 
	{

		missionNumber = 1; 
		moneyCount = 0;
		mission.text = "Head to Island Cove to retrieve your buried treasure. Avoid the enemy cannon fire.";
	}
	
	// Update is called once per frame
	void Update () 
	{
		UpdateMoney();
		if (missionNumber == 1 && location.text == "Island Cove")
		{
			missionNumber++;
			NeedleControler dest = destArrow.GetComponent<NeedleControler>();
			dest.player = england;
			moneyCount += 1000;
			mission.text = "Treasure Collected! Now head to england to purchase cannons.";

		}
		else if (missionNumber == 2 && location.text == "Port of England")
		{
			missionNumber++;
			this.GetComponent<BoatController>().hasCannons = true;
			moneyCount -= 500;
			mission.text = "Cannons Purchased! Use space to fire, right/left click to aim.\nDestroy all 5 enemies.";


		}
		else if (missionNumber == 3 && GameObject.FindGameObjectsWithTag("Destroyable").Length == 0)
		{

			missionNumber++;
			mission.text = "Congratulations, you are the winner!";
		}
	
	}

	void UpdateMoney()
	{
		money.text = "$" + moneyCount;
	}
}
