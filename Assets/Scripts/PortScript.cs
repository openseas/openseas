﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class PortScript : MonoBehaviour {

	public string portName;
	public Text location;
	private string openSeas = "Open Sea";
	private string ship = "Ship";

	// Use this for initialization
	void Start () 
	{
		location.text = openSeas;
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnTriggerEnter(Collider other)
	{
		if(other.gameObject.name == ship)
		{
			location.text =  portName;

		}

	}

	void OnTriggerExit(Collider other)
	{
		if(other.gameObject.name == ship)
		{
			location.text =  openSeas;

		}
	}
}
