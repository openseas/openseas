﻿using UnityEngine;
using System.Collections;

public class fireCannon : MonoBehaviour {

	public GameObject cannonball;
	public float rateOfFire = 0.5f;
	private float fireDelay;
	public float speed = 20;
	public BoatController bc;

	// Update is called once per frame
	void Update () 
	{
		if(Input.GetKey(KeyCode.Space) && Time.time > fireDelay && bc.hasCannons)
		{
			fireDelay = Time.time + rateOfFire;
			GameObject clone = (GameObject)Instantiate(cannonball, transform.position, transform.rotation);
			clone.rigidbody.velocity = transform.TransformDirection(new Vector3(0,0,speed));
			Physics.IgnoreCollision(clone.collider, transform.root.collider);
			 
		}

	}
}
