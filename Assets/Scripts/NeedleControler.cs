﻿using UnityEngine;
using System.Collections;

public class NeedleControler : MonoBehaviour {

	public GameObject player;
	Vector3 offset;


	void Start()
	{
		offset = transform.position;
	}


	
	void LateUpdate ()
	{

		transform.position = player.transform.position + offset;
		transform.rotation = player.transform.rotation;
		
	}
}
