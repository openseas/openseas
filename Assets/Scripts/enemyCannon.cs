﻿using UnityEngine;
using System.Collections;

public class enemyCannon : MonoBehaviour {

	public GameObject cannonball;
	public float rateOfFire = 0.5f;
	private float fireDelay;
	public float speed = 20;
	public GameObject target;
	private float distanceToBeginFire;
	public bool fire;


	void Start()
	{
		distanceToBeginFire = 300;
		fire = true;
	}


	// Update is called once per frame
	void Update () 
	{
		float distance = Vector3.Distance(this.transform.position, target.transform.position);
		if( Time.time > fireDelay && Time.time > Random.Range(0.0f, 5.0f) && distance < distanceToBeginFire && fire)
		{
			fireDelay = Time.time + rateOfFire;
			GameObject clone = (GameObject)Instantiate(cannonball, transform.position, transform.rotation);
			clone.rigidbody.velocity = transform.TransformDirection(new Vector3(0,0,speed));
			Physics.IgnoreCollision(clone.collider, transform.root.collider);
			
		}
	
	}
}
